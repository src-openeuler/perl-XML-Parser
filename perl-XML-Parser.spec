%define mod_name XML-Parser

Name:        perl-%{mod_name}
Epoch:       1
Version:     2.46
Release:     2
Summary:     Perl module for parsing XML documents
License:     GPLv1+ or Artistic 1.0
URL:         https://metacpan.org/release/%{mod_name}
Source0:     https://cpan.metacpan.org/authors/id/T/TO/TODDR/%{mod_name}-%{version}.tar.gz

BuildRequires:  gcc make perl-generators perl expat-devel perl-devel
Requires:       perl

%description
This module provides ways to parse XML documents. It is built on
top of XML::Parser::Expat, which is a lower level interface to
James Clark's expat library. Each call to one of the parsing
methods creates a new instance of XML::Parser::Expat which is
then used to parse the document. Expat options may be provided
when the XML::Parser object is created. These options are then
passed on to the Expat object on each parse call. They can also
be given as extra arguments to the parse methods, in which case
they override options given at XML::Parser creation time.

%package help
Summary: Man page for %{name}
BuildArch: noarch

%description help
%{summary}.

%prep
%autosetup -n %{mod_name}-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} %{buildroot}/*

%check
make test

%files
%defattr(-,root,root)
%doc Changes README samples/
%{perl_vendorarch}/XML/
%{perl_vendorarch}/auto/XML/

%files help
%{_mandir}/man3/*.3*

%changelog
* Wed Oct 26 2022 wangyuhang <wangyuhang27@huawei.com> - 1:2.46-2
- define mod_name to opitomize the specfile

* Wed Jul 22 2020 dingyue <dingyue5@huawei.com> - 1:2.46-1
- update to 2.46

* Wed May 13 2020 licunlong <licunlong1@huawei.com> - 1:2.44-4
- add perl-devel buildrequire

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:2.44-3
- Change mod of files

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:2.44-2
- Adjust requires

* Fri Sep 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:2.44-1
- Package init
